# -*- coding: utf-8 -*-
"""
Created by:

Kai Sandvold Beckwith
Ellenberg group
EMBL Heidelberg
"""

from looptrace import image_processing_functions as ip
from looptrace import image_io
import dask.array as da
import os
import numpy as np
import pandas as pd
from skimage.segmentation import expand_labels, relabel_sequential
from skimage.measure import regionprops_table
from skimage.transform import rescale
from skimage.morphology import remove_small_objects
from scipy import ndimage as ndi
import tqdm

class NucDetector:
    '''
    Class for handling generation and detection of e.g. nucleus images.
    '''

    def __init__(self, image_handler):
        self.image_handler = image_handler
        self.config = image_handler.config
        try:
            self.images = self.image_handler.images[self.config['nuc_input_name']]
            self.pos_list = self.image_handler.image_lists[self.config['nuc_input_name']]
        except KeyError:
            pass
        try:
            self.zipstore = self.config['zipstore']
        except KeyError:
            self.zipstore = False

        self.nuc_images_path = self.image_handler.image_save_path+os.sep+'nuc_images'
        self.nuc_masks_path = self.image_handler.image_save_path+os.sep+'nuc_masks'
        self.nuc_classes_path = self.image_handler.image_save_path+os.sep+'nuc_classes'

    def gen_nuc_images(self):
        '''
        Saves 2D/3D (defined in config) images of the nuclear channel into image folder for later analysis.
        '''
        try:
            nuc_slice = self.config['nuc_slice']
        except KeyError: #Legacy config
            nuc_slice = -1
        try:
            nuc_3d = self.config['nuc_3d']
        except KeyError:
            nuc_3d = False
        
        print('Generating nuclei images.')
        imgs = []
        for i, pos in tqdm.tqdm(enumerate(self.pos_list)):
            if nuc_3d:
                img = self.images[i][self.config['nuc_ref_frame'], self.config['nuc_channel']].compute()
            elif nuc_slice == -1:
                img = da.max(self.images[i][self.config['nuc_ref_frame'], self.config['nuc_channel']], axis=0).compute()
            else:
                img = self.images[i][self.config['nuc_ref_frame'], self.config['nuc_channel'], self.config['nuc_slice']].compute()

            if nuc_3d:
                image_io.single_position_to_zarr(img, path=self.nuc_images_path, name = 'nuc_images', pos_name=pos, axes=('z','y','x'), dtype=np.uint16, chunk_split=(1,1), zipstore=self.zipstore)
            else:
                image_io.single_position_to_zarr(img, path=self.nuc_images_path, name = 'nuc_images', pos_name=pos, axes=('y','x'), dtype=np.uint16, chunk_split=(1,1), zipstore = self.zipstore)

    def segment_nuclei(self):
        seg_method = self.config['nuc_method']
        if seg_method == 'threshold':
            print('Segmenting nuclei by threshold method.')
            self.segment_nuclei_threshold()
        elif seg_method == 'rf':
            print('Segmenting nuclei by random forest method.')
            self.segment_nuclei_rf()
        elif (seg_method == 'cellpose') or (seg_method == 'nuclei'):
            print('Segmenting nuclei by Cellpose.')
            self.segment_nuclei_cellpose()
        else:
            print('Selected segmentation method not implemented.')

    def segment_nuclei_rf(self):
        from skimage.segmentation import expand_labels
        from skimage.transform import rescale
        import pickle
        from skimage import feature, future
        from functools import partial
        import tqdm

        sigma_min = 1
        sigma_max = 16
        features_func = partial(feature.multiscale_basic_features,
                            intensity=True, edges=False, texture=True,
                            sigma_min=sigma_min, sigma_max=sigma_max)

        clf = pickle.load(open(os.path.dirname(self.image_handler.out_path)+os.sep+'nuc_rf_model.pickle', 'rb'))
        for i, pos in tqdm.tqdm(enumerate(self.pos_list)):
            img = np.array(self.image_handler.images['nuc_images'][i][0,0,::2,::2,::2])
            features = features_func(img)
            mask = future.predict_segmenter(features, clf)
            mask = mask - 1
            mask = ndi.label(mask)[0]
            mask = rescale(expand_labels(mask.astype(np.uint16),self.config['nuc_dilation']), scale = (2, 2, 2), order = 0)
            image_io.single_position_to_zarr(mask, path = self.nuc_masks_path, name = 'nuc_masks', pos_name=pos, axes=('z','y','x'), dtype = np.uint16, chunk_split=(1,1), zipstore=self.zipstore)

    def segment_nuclei_threshold(self):
        for i, pos in tqdm.tqdm(enumerate(self.pos_list)):
            img = np.array(self.image_handler.images['nuc_images'][i][0,0,::2,::2,::2])
            img = ndi.gaussian_filter(img, 2)
            mask = img > int(self.config['nuc_threshold'])
            mask = ndi.binary_fill_holes(mask)
            mask = remove_small_objects(mask, min_size=self.config['nuc_min_size']).astype(np.uint16)
            mask = ndi.label(mask)[0]
            mask = rescale(expand_labels(mask.astype(np.uint16),self.config['nuc_dilation']), scale = (2, 2, 2), order = 0)
            image_io.single_position_to_zarr(mask, path = self.nuc_masks_path, name = 'nuc_masks', pos_name=pos, axes=('z','y','x'), dtype = np.uint16, chunk_split=(1,1), zipstore = self.zipstore)

    def segment_nuclei_cellpose(self):
        '''
        Runs nucleus segmentation using nucleus segmentation algorithm defined in ip functions.
        Dilates a bit and saves images.
        '''
        
        print('Segmenting nuclei.')
        
        try:
            method = self.config['nuc_cellpose_model']
        except KeyError:
            method = 'nuclei'
        try:
            nuc_3d = self.config['nuc_3d']
            ds_xy = self.config['nuc_downscaling_xy']
            
            if nuc_3d:
                anisotropy = self.config['nuc_anisotropy']
                ds_z = self.config['nuc_downscaling_z']
                nuc_min_size = self.config['nuc_min_size']/(ds_z*ds_xy*ds_xy)
            else:
                nuc_min_size = self.config['nuc_min_size']/(ds_xy*ds_xy)
                ds_z = 1
                
            mitosis_class = self.config['nuc_mitosis_class']
        except KeyError:
            nuc_3d = False
            ds_xy = 4
            ds_z = 1
            nuc_min_size = 10
            mitosis_class = False
        
        diameter = self.config['nuc_diameter']/ds_xy
        
        print('Segmenting nuclei.')
        nuc_imgs_in = self.image_handler.images['nuc_images']
        nuc_imgs = []

        #Remove unused dimensions and downscale input images.        
        for i, pos in tqdm.tqdm(enumerate(self.pos_list)):
            nuc_imgs.append(np.array(self.image_handler.images['nuc_images'][i][0,0,::ds_z,::ds_xy,::ds_xy]))
        print(nuc_imgs[0].shape)
        print(nuc_3d)
        print(f'Running nuclear segmentation using CellPose {method} model and diameter {diameter}.')
        
        for i, pos in tqdm.tqdm(enumerate(self.pos_list)):

            nuc_img = np.array(self.image_handler.images['nuc_images'][i][0,0,::ds_z,::ds_xy,::ds_xy])
            if nuc_3d:
                mask = ip.nuc_segmentation_cellpose_3d(nuc_img, diameter = diameter, model_type = method, anisotropy=anisotropy)
            else:
                mask = ip.nuc_segmentation_cellpose_2d(nuc_img, diameter = diameter, model_type = method)[0]

            #Remove under-segmented nuclei and clean up:
            mask = remove_small_objects(mask, min_size=nuc_min_size)
            mask = relabel_sequential(mask)[0]

            if mitosis_class:
                print(f'Detecting mitotic cells on top of CellPose nuclei.')
                mask, mitotic_idx = ip.mitotic_cell_extra_seg(np.array(nuc_img), mask)

            if nuc_3d:
                mask = rescale(expand_labels(mask.astype(np.uint16),self.config['nuc_dilation']), scale = (ds_z, ds_xy, ds_xy), order = 0)
            else:
                mask = rescale(expand_labels(mask.astype(np.uint16),self.config['nuc_dilation']), scale = (ds_xy, ds_xy), order = 0)
            #masks = np.stack(masks)

            if nuc_3d:
                image_io.single_position_to_zarr(mask, path = self.nuc_masks_path, name = 'nuc_masks', pos_name=pos, axes=('z','y','x'), dtype = np.uint16, chunk_split=(1,1), zipstore = self.zipstore)
            else:
                image_io.single_position_to_zarr(mask, path = self.nuc_masks_path, name = 'nuc_masks', pos_name=pos, axes=('y','x'), dtype = np.uint16, chunk_split=(1,1), zipstore = self.zipstore)

        # if mitosis_class:
        #     nuc_class = []
        #     for i, mask in enumerate(masks):
        #         class_1 = ((mask > 0) & (mask < mitotic_idx[i])).astype(int)
        #         class_2 = (mask >= mitotic_idx[i]).astype(int)
        #         nuc_class.append(class_1 + class_2*2)
        #     #nuc_class = np.stack(nuc_class).astype(np.uint16)
        #     print('Saving classifications.')

        #     self.image_handler.images['nuc_classes'] = nuc_class
        #     if nuc_3d:
        #         image_io.images_to_ome_zarr(images=nuc_class, path=self.nuc_classes_path, name='nuc_classes', axes=('p','z', 'y','x'), dtype = np.uint16, chunk_split=(1,1), zipstore = self.zipstore)
        #     else:
        #         image_io.images_to_ome_zarr(images=nuc_class, path=self.nuc_classes_path, name='nuc_classes', axes=('p','y','x'), dtype = np.uint16, chunk_split=(1,1), zipstore = self.zipstore)

    def classify_nuclei(self, default_class = 1):
        '''
        #from sklearn.decomposition import PCA
        from sklearn.cluster import KMeans

        all_props = []
        for i, pos in tqdm.tqdm(enumerate(self.pos_list)):
            try:
                nuc_img = np.array(self.image_handler.images['nuc_images'][i][0,0,::2,::2,::2])
                nuc_mask = np.array(self.image_handler.images['nuc_masks'][i][0,0,::2,::2,::2])
            except KeyError:
                print("Need to segment nuclei first.")
                return
            props = pd.DataFrame(regionprops_table(nuc_mask, nuc_img, properties=('label', 'centroid_weighted', 'intensity_max', 'intensity_mean')))
            props['position'] = pos
            all_props.append(props)
        
        all_props = pd.concat(all_props).reset_index(drop=True)
        #print(all_props)
        if self.config['nuc_3d']:
            X = all_props[['centroid_weighted-0', 'intensity_max', 'intensity_mean']].to_numpy()
        else:
            X = all_props[['intensity_max', 'intensity_mean']].to_numpy()

        all_props['classes'] = KMeans(n_clusters=2, random_state=0, n_init="auto").fit_predict(X) + 1
        #print(all_props)

        for i, pos in tqdm.tqdm(enumerate(self.pos_list)):
            nuc_mask = np.array(self.image_handler.images['nuc_masks'][i][0,0,::2,::2,::2])
            props = all_props[all_props.position == pos]
            labels = all_props.label.to_numpy()
            classes = all_props.classes.to_numpy()
            index = np.searchsorted(labels,nuc_mask)
            nuc_class = classes[index].reshape(nuc_mask.shape)
            nuc_class = rescale(nuc_class.astype(np.uint16), scale = (2, 2, 2), order = 0)
            image_io.single_position_to_zarr(nuc_class, path = self.nuc_classes_path, name = 'nuc_class', pos_name=pos, axes=('z','y','x'), dtype = np.uint16, chunk_split=(1,1))
        '''
        for i, pos in tqdm.tqdm(enumerate(self.pos_list)):
            nuc_mask = np.array(self.image_handler.images['nuc_masks'][i][0,0])
            nuc_class = nuc_mask.copy()
            nuc_class[nuc_class > 0] = int(default_class)
            #nuc_class = np.clip(nuc_mask, 0, 1)
            image_io.single_position_to_zarr(nuc_class, path = self.nuc_classes_path, name = 'nuc_class', pos_name=pos, axes=('z','y','x'), dtype = np.uint16, chunk_split=(1,1), zipstore = self.zipstore)

    def update_masks_after_qc(self, new_mask, original_mask, mask_name, position):

        try:
            nuc_3d = self.config['nuc_3d']
        except KeyError:
            nuc_3d = False
        #s = tuple([slice(None, None, 4) for i in range(new_mask.ndim)])
        #if not np.allclose(new_mask[s], original_mask[s]):
            #print('Segmentation labels changed, resaving.')
            #nuc_mask = ip.relabel_nucs(new_mask)
        pos_index = self.image_handler.image_lists[mask_name].index(position)
        #self.image_handler.images[mask_name] = nuc_mask.astype(np.uint16)
        if nuc_3d:
            image_io.single_position_to_zarr(images = self.image_handler.images[mask_name][pos_index], path=self.nuc_masks_path+position, pos_name = position, name = mask_name, axes=('z','y','x'), dtype=np.uint16, chunk_split=(1,1), zipstore = self.zipstore)
        else:
            image_io.single_position_to_zarr(images = self.image_handler.images[mask_name][pos_index], path=self.nuc_masks_path+position, pos_name = position, name = mask_name, axes=('y','x'), dtype=np.uint16, chunk_split=(1,1), zipstore = self.zipstore)


    def gen_nuc_rois(self):
        
        def calc_pad(c, c_max):
        #Helper function to calculate padding due to drift.
            if c<0:
                return abs(c)
            elif c>c_max:
                return c-c_max
            else:
                return 0
        def constrain_coords(c, c_max):
            if c<0:
                return 0
            elif c>c_max:
                return c_max
            else:
                return c

        #Use if images are not drift corrected, but a drift correction needs to have been calculated using Drifter.
        nuc_rois = []
        nuc_masks = self.image_handler.images['nuc_masks']
        nuc_drifts = self.image_handler.tables[self.image_handler.config['nuc_input_name']+'_drift_correction']
        spot_drifts = self.image_handler.tables[self.image_handler.config['spot_input_name']+'_drift_correction']
        #ch = self.config['trace_ch']
        #ref_frame = self.config['nuc_ref_frame']
        
        for i, pos in tqdm.tqdm(enumerate(self.pos_list)):
            sel_nuc_drift = nuc_drifts.query('position == @pos').iloc[0]
            sel_spot_drift = spot_drifts.query('position == @pos')

            mask = np.array(nuc_masks[i][0,0])

            if 'nuc_classes' in self.image_handler.images:
                nuc_class = np.array(self.image_handler.images['nuc_classes'][i][0,0])
            else:
                nuc_class = mask > 0

            if mask.ndim == 2:
                nuc_props = pd.DataFrame(regionprops_table(mask, intensity_image=nuc_class, properties=['label', 'bbox', 'intensity_mean'])).rename(columns={'bbox-0':'y_min', 
                                                                                'bbox-1':'x_min', 
                                                                                'bbox-2':'y_max', 
                                                                                'bbox-3':'x_max'})
            else:
                nuc_props = pd.DataFrame(regionprops_table(mask, intensity_image=nuc_class, properties=['label', 'bbox', 'intensity_mean'])).rename(columns={'bbox-0':'z_min', 
                                                                'bbox-1':'y_min', 
                                                                'bbox-2':'x_min', 
                                                                'bbox-3':'z_max', 
                                                                'bbox-4':'y_max', 
                                                                'bbox-5':'x_max'})

            for j, roi in nuc_props.iterrows():
                new_pos = pos+'_'+str(j+1).zfill(4)+'.zarr'

                try:
                    Z, Y, X = self.images[i][0,0].shape[-3:]
                except AttributeError: #Images not loaded for some reason
                    Z = 200
                    Y = nuc_masks[0].shape[-2]
                    X = nuc_masks[0].shape[-1]
                
                for k, dc_frame in sel_spot_drift.iterrows():
                    z_drift_course = int(dc_frame['z_px_course']) - int(sel_nuc_drift['z_px_course'])
                    y_drift_course = int(dc_frame['y_px_course']) - int(sel_nuc_drift['y_px_course']) 
                    x_drift_course = int(dc_frame['x_px_course']) - int(sel_nuc_drift['x_px_course'])

                    if nuc_masks[0].ndim == 2:
                        z_min = 0
                        z_max = Z
                    else:
                        z_min = constrain_coords(int(roi['z_min'] - z_drift_course), Z)
                        z_max = constrain_coords(int(roi['z_max'] - z_drift_course), Z)
                        
                    y_min = constrain_coords(int(roi['y_min'] - y_drift_course), Y)
                    y_max = constrain_coords(int(roi['y_max'] - y_drift_course), Y)
                    x_min = constrain_coords(int(roi['x_min'] - x_drift_course), X)
                    x_max = constrain_coords(int(roi['x_max'] - x_drift_course), X)

                    #Handling case of ROI extending beyond image edge after drift correction:
                    if nuc_masks[0].ndim == 2:
                        pad_z_min = 0
                        pad_z_max = 0
                    else:
                        pad_z_min = calc_pad(int(roi['z_min'] - z_drift_course), Z)
                        pad_z_max = calc_pad(int(roi['z_max'] - z_drift_course), Z)
                    pad_y_min = calc_pad(int(roi['y_min'] - y_drift_course), Y)
                    pad_y_max = calc_pad(int(roi['y_max'] - y_drift_course), Y)
                    pad_x_min = calc_pad(int(roi['x_min'] - x_drift_course), X)
                    pad_x_max = calc_pad(int(roi['x_max'] - x_drift_course), X)

                    nuc_rois.append([pos, new_pos, i, roi.name, dc_frame['frame'],  z_min, z_max, y_min, y_max, x_min, x_max,
                                                                                    pad_z_min, pad_z_max, pad_y_min, pad_y_max, pad_x_min, pad_x_max, 
                                                                                    z_drift_course, y_drift_course, x_drift_course, 
                                                                                    dc_frame['z_px_fine'], dc_frame['y_px_fine'], dc_frame['x_px_fine'], roi['intensity_mean']])

        nuc_rois = pd.DataFrame(nuc_rois, columns=['orig_position','position', 'orig_pos_index', 'roi_id', 'frame', 
                                                    'z_min', 'z_max', 'y_min', 'y_max', 'x_min', 'x_max', 
                                                    'pad_z_min', 'pad_z_max', 'pad_y_min', 'pad_y_max', 'pad_x_min', 'pad_x_max', 
                                                    'z_px_course', 'y_px_course', 'x_px_course', 
                                                    'z_px_fine', 'y_px_fine', 'x_px_fine', 'nuc_class'])
        nuc_rois['nuc_class'] = nuc_rois['nuc_class'].round()
        self.nuc_rois = nuc_rois
        self.nuc_rois.to_csv(self.image_handler.out_path+'nuc_rois.csv')
        nuc_dc = nuc_rois[['frame', 'position', 'z_px_course', 'z_px_fine','y_px_course', 'y_px_fine', 'x_px_course', 'x_px_fine']].copy()
        nuc_dc['z_px_course'] = 0
        nuc_dc['y_px_course'] = 0
        nuc_dc['x_px_course'] = 0
        nuc_dc.to_csv(self.image_handler.out_path+self.config['nuc_extract_name']+'_single_nucs_drift_correction.csv')



    def gen_nuc_rois_prereg(self):
        #Use if nuclei images are already registered to rest of images you want to extract.

        nuc_rois = []
        nuc_masks = self.image_handler.images['nuc_masks']
        #ch = self.config['trace_ch']
        #ref_frame = self.config['nuc_ref_frame']
        
        for i in tqdm.tqdm(range(len(nuc_masks))):

            mask = np.array(nuc_masks[i][0,0])
            if mask.shape[0] == 1:
                nuc_props = pd.DataFrame(regionprops_table(mask, properties=['label', 'bbox', 'area'])).rename(columns={'bbox-0':'y_min', 
                                                                                'bbox-1':'x_min', 
                                                                                'bbox-2':'y_max', 
                                                                                'bbox-3':'x_max'})
            else:
                nuc_props = pd.DataFrame(regionprops_table(mask, properties=['label', 'bbox', 'area'])).rename(columns={'bbox-0':'z_min', 
                                                                'bbox-1':'y_min', 
                                                                'bbox-2':'x_min', 
                                                                'bbox-3':'z_max', 
                                                                'bbox-4':'y_max', 
                                                                'bbox-5':'x_max'})

            nuc_props['orig_position'] = self.image_handler.image_lists['nuc_masks'][i]
            nuc_props['position'] = self.image_handler.image_lists['nuc_masks'][i]+'_'+'P'+nuc_props['label'].apply(str).str.zfill(3)+'.zarr'
            nuc_rois.append(nuc_props)

        self.nuc_rois = pd.concat(nuc_rois).reset_index(drop=True)
        self.nuc_rois.to_csv(self.image_handler.out_path+'nuc_rois.csv')
                
                
    def extract_single_roi_img(self, single_roi, images):
        # Function for extracting a single cropped region defined by ROI from a larger 3D image.

        z = slice(single_roi['z_min'], single_roi['z_max'])
        y = slice(single_roi['y_min'], single_roi['y_max'])
        x = slice(single_roi['x_min'], single_roi['x_max'])
        pad = ( (single_roi['pad_z_min'], single_roi['pad_z_max']),
                (single_roi['pad_y_min'], single_roi['pad_y_max']),
                (single_roi['pad_x_min'], single_roi['pad_x_max']))

        try:
            roi_img = np.array(images[z, y, x])

            #If microscope drifted, ROI could be outside image. Correct for this:
            if pad != ((0,0),(0,0),(0,0)):
               roi_img = np.pad(roi_img, pad, mode='minimum')

        except (ValueError, TypeError): # ROI collection failed for some reason
            roi_img = np.zeros((z.stop-z.start,y.stop-y.start,x.stop-x.start), dtype=np.float32)

        return roi_img  #{'p':p, 't':t, 'c':c, 'z':z, 'y':y, 'x':x, 'img':roi_img}

    def gen_single_nuc_images(self):
        nuc_extract_name = self.config['nuc_extract_name']
        #Function to extract single nuclei images from full FOVs
        # A bit convoluted currently to ensure efficient loading of appropriate arrays into RAM for processing.
        print('Extracting nucs from ', nuc_extract_name)
        rois = self.image_handler.tables['nuc_rois']#.iloc[0:500]
        #rois_orig_all_pos = sorted(list(rois.orig_position.unique()))
        input_images = self.image_handler.images[nuc_extract_name]

        T = input_images[0].shape[0]
        C = input_images[0].shape[1]
        
        for pos in self.pos_list:
            roi_array = {}
            print("extracting nucs in position ", pos)
            pos_index = self.image_handler.image_lists[nuc_extract_name].index(pos)
            rois_orig_pos = rois[rois.orig_position == pos]

            for frame in tqdm.tqdm(range(T)):
                image_stack = np.array(input_images[pos_index][frame])
                for i, pos_roi in rois_orig_pos[rois_orig_pos.frame==frame].iterrows():
                    try:
                        roi_array[pos_roi['position'], frame] = np.stack([self.extract_single_roi_img(pos_roi, image_stack[c]).astype(np.uint16) for c in range(C)])
                    except ValueError as e:
                        print('Something wrong with image, saving zeros:', e)
                        roi_array[pos_roi['position'], frame] = np.zeros(shape = (C,10,10,10))

        
            for nuc_position in tqdm.tqdm(list(rois_orig_pos.position.unique())):
                try:
                    nuc_pos_images = np.stack([roi_array[(nuc_position, frame)] for frame in range(T)])
                    #print('Made pos roi ', roi_id, pos_rois.shape)
                except KeyError:
                    break
                except ValueError:
                    nuc_pos_images = np.zeros(shape = (T,C,10,10,10))

                image_io.single_position_to_zarr(images=nuc_pos_images, 
                            path=self.image_handler.image_save_path+os.sep+nuc_extract_name+'_single_nucs', 
                            name='single_nucs',
                            pos_name = nuc_position, 
                            axes=('t','c','z','y','x'), 
                            chunk_axes = ('t','z','y','x'),
                            dtype = np.uint16,
                            metadata=  self.image_handler.image_metadata[nuc_extract_name],
                            chunk_split=(1,1,1,1),
                            zipstore = self.zipstore)
        print('ROI images generated, please reinitialize to load them.')
