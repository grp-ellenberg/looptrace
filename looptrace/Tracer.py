# -*- coding: utf-8 -*-
"""
Created by:

Kai Sandvold Beckwith
Ellenberg group
EMBL Heidelberg
"""

import numpy as np
import pandas as pd
import scipy.ndimage as ndi
import looptrace.image_processing_functions as ip
#from looptrace import image_io
from looptrace.gaussfit import fitSymmetricGaussian3D, fitSymmetricGaussian3DMLE
from tqdm import tqdm
from skimage.feature import peak_local_max
#import os

class Tracer:
    def __init__(self, image_handler, roi_name = None):
        '''
        Initialize Tracer class with config read in from YAML file.
    '''
        self.image_handler = image_handler
        self.config_path = image_handler.config_path
        self.config = image_handler.config
        self.drift_table = image_handler.tables[self.config['spot_input_name']+'_drift_correction']
        self.images = self.image_handler.images[self.config['trace_input_name']]
            
        self.pos_list = self.image_handler.image_lists[self.config['spot_input_name']]
        

        if roi_name == None:
            self.roi_name = self.config['spot_input_name']
        else:
            self.roi_name = roi_name

        self.roi_table = image_handler.tables[self.roi_name+'_rois']

        self.all_rois = image_handler.tables[self.roi_name+'_dc_rois']

        self.traces_path = self.image_handler.out_path+self.roi_name+'_traces.csv'

        self.fit_funcs = {'LS': fitSymmetricGaussian3D, 'MLE': fitSymmetricGaussian3DMLE}
        self.fit_func = self.fit_funcs[self.config['fit_func']]


        if self.image_handler.pos_id is not None:
            self.roi_table = self.roi_table[self.roi_table.position.isin(self.pos_list)]
            self.all_rois = self.all_rois[self.all_rois.position.isin(self.pos_list)].reset_index(drop=True)
            self.traces_path = self.image_handler.out_path+self.roi_name+'_traces.csv'[:-4]+'_'+str(self.image_handler.pos_id).zfill(4)+'.csv'
            self.images = self.images[self.roi_table.index.to_list()]


    def trace_single_roi(self, roi_img, mask = None, background = None):
        
        #Fit a single roi with 3D gaussian (MLE or LS as defined in config).
        #Masking by intensity or label image can be used to improve fitting correct spot (set in config)

        if background is not None:
            roi_img = roi_img - background
        if np.any(roi_img) and np.all([d > 2 for d in roi_img.shape]): #Check if empty or too small for fitting
            if mask is None:
                fit = self.fit_func(roi_img, sigma=1, center='max')[0]
            else:
                roi_img_masked = (mask/np.max(mask))**2 * roi_img
                center = list(np.unravel_index(np.argmax(roi_img_masked, axis=None), roi_img.shape))
                fit = self.fit_func(roi_img, sigma=1, center=center)[0]
            return fit
        else:
            fit = np.array([-1, -1, -1, -1, -1, -1, -1])
            return fit
        

    def trace_all_rois(self):
        '''
        Fits 3D gaussian to previously detected ROIs across positions and timeframes.
    
        '''
        imgs = self.images

        fits = []

        #fits = Parallel(n_jobs=-1, prefer='threads')(delayed(self.trace_single_roi)(roi_imgs[i]) for i in tqdm(range(roi_imgs.shape[0])))
        try:
            mask_fits = self.image_handler.config['mask_fits']
        except KeyError:
            mask_fits = False

        try:
            subtract_background = self.image_handler.config['substract_background']
            if type(subtract_background) == 'int': #Legacy config mode
                background_frame = self.image_handler.config['substract_background']
                subtract_background = True

            if subtract_background:
                try:
                #This only works for a single position at the time currently
                    background_frame = self.image_handler.config['trace_background_frame'] #Frame to subtract background from.
                except KeyError:
                    pass
                pos_drifts = self.drift_table[self.drift_table.position.isin(self.pos_list)][['z_px_fine', 'y_px_fine', 'x_px_fine']].to_numpy()
                background_rel_drifts = pos_drifts - pos_drifts[background_frame]
            

        except KeyError:
            subtract_background = False
        


        try:
            fit_multi = self.image_handler.config['fit_multi']
        except KeyError:
            fit_multi = False

        if not fit_multi:
            ref_frames = self.roi_table['frame'].to_list()
            ref_chs = self.roi_table['ch'].to_list()
            for p, pos_imgs in tqdm(enumerate(imgs), total=len(imgs)):
                if mask_fits:
                    try:
                        ref_img = pos_imgs[ref_frames[p], ref_chs[p]]
                    except IndexError: #Edge case, if images used for tracing (e.g. deconvolved) have fewer channels than the images used for spot detection, channels might be incorrect and defaults to 0 instead.
                        ref_img = pos_imgs[ref_frames[p], 0]
                else:
                    ref_img = None
                #print(ref_img.shape)
                for t, frame_img in enumerate(pos_imgs):
                    if frame_img.ndim == 3: #Legacy, only one channel traced and spot images contains no channel axis.
                        if subtract_background:
                            try:
                                frame_img = np.clip(frame_img.astype(np.int16) - ndi.shift(pos_imgs[background_frame], shift = background_rel_drifts[t], mode='nearest').astype(np.int16), a_min = 0, a_max = None).astype(np.uint16)
                            except ValueError:
                                fits.append(np.array([-1, -1, -1, -1, -1, -1, -1]))
                                continue
                        fits.append(self.trace_single_roi(frame_img, mask = ref_img))
                        
                    elif frame_img.ndim == 4: #Up to date, compatible with single or multichannel tracing.
                        for ch, spot_img in enumerate(frame_img):
                            if subtract_background:
                                try:
                                    spot_img = np.clip(spot_img.astype(np.int16) - ndi.shift(pos_imgs[background_frame, ch], shift = background_rel_drifts[t], mode='nearest').astype(np.int16), a_min = 0, a_max = None).astype(np.uint16)
                                except ValueError:
                                    fits.append(np.array([-1, -1, -1, -1, -1, -1, -1]))
                                    continue
                            fits.append(self.trace_single_roi(spot_img, mask = ref_img))

                #Parallel(n_jobs=1, prefer='threads')(delayed(self.trace_single_roi)(imgs[p, t], mask= ref_img) for t in range(imgs.shape[1]))

        elif fit_multi: #new fitting flag
            for p, pos_imgs in tqdm(enumerate(imgs), total=len(imgs)):
                n_fits = {}
                pos_fits = {}                    

                for t, frame_img in enumerate(pos_imgs):
                    for ch, spot_img in enumerate(frame_img): #Loop over ROIs, frames and channels.
                        try:
                            if subtract_background:
                                #background_frame = ndi.shift(pos_imgs[background_frame, ch], shift = background_rel_drifts[t], order=0)
                                spot_img = np.clip(spot_img.astype(np.int16) - ndi.shift(pos_imgs[background_frame, ch], shift = background_rel_drifts[t], mode='nearest').astype(np.int16), a_min = 0, a_max = None).astype(np.uint16)
                            peaks = peak_local_max(spot_img, threshold_rel=0.5, threshold_abs=np.median(spot_img)*1.2, min_distance=2, num_peaks=3) #detect possible peaks
                        except ValueError: #Something wrong with the image
                            n_fits[t,ch] = 0
                            pos_fits[t,ch] = [np.array([-1, -1, -1, -1, -1, -1, -1])]
                            continue
                        spot_fits=[]
                        #peak_imgs = []
                        bead_roi_px = 4
                        if len(peaks) == 0: #If no peaks found
                            n_fits[t,ch] = 0
                            pos_fits[t,ch] = [np.array([-1, -1, -1, -1, -1, -1, -1])] #Dummy fit if no peaks found
                        else:
                            for peak in peaks: #If peaks found.
                                peak_img = ip.extract_single_bead(peak, spot_img, bead_roi_px=bead_roi_px) #Extract small roi around peak
                                #peak_imgs.append(peak_img)
                                fit, _ = fitSymmetricGaussian3D(peak_img, sigma=1, center=None) #Fit peak in small ROI.
                                fit[2:5] = fit[2:5]+peak-bead_roi_px//2 #Scale coordinates to original ROI.
                                spot_fits.append(fit)
                            spot_fits = np.array(spot_fits)
                            spot_fits = spot_fits[~np.any(spot_fits<=0, axis=1)] #Filter bad fits
                            spot_fits = spot_fits[(spot_fits[:,1]/spot_fits[:,0]) > 0.5] #Filter low quality fits
                            if spot_fits.shape[0] == 0: #If all fits removed by filter.
                                n_fits[t,ch] = 0
                                pos_fits[t,ch] = [np.array([-1, -1, -1, -1, -1, -1, -1])] #Dummy fit if no peaks found.
                            else:
                                n_fits[t,ch]=(len(spot_fits))
                                pos_fits[t,ch] = spot_fits
                            #print(fits)

                tmp_fits = np.concatenate([i for i in pos_fits.values() if len(i)>0]) 
                med_pos = np.median(tmp_fits[~np.any(tmp_fits<=0, axis=1)], axis=0) #Calculate median of all remaining proper fits.

                for key, val in n_fits.items():
                    if val>1:
                        dists = []
                        for f in pos_fits[key]:
                            dists.append(np.sqrt(np.sum((f[2:5]-med_pos[2:5])**2))) #Calculate distance from ambigous fits to median.
                        pos_fits[key] = [pos_fits[key][np.argmin(dists)]] #keep only ambiguous fit closest to median.
                
                fits.append(np.concatenate([i for i in pos_fits.values() if len(i)>0]))
            fits = np.concatenate(fits) #Put all together.

        trace_res = pd.DataFrame(fits,columns=["BG","A","z_px","y_px","x_px","sigma_z","sigma_xy"])
        #trace_index = pd.DataFrame(fit_rois, columns=["trace_id", "frame", "ref_frame", "position", "drift_z", "drift_y", "drift_x"])
        traces = pd.concat([self.all_rois, trace_res], axis=1)
        traces['trace_id'] = traces['roi_id']
        #traces.rename(columns={"roi_id": "trace_id"}, inplace=True)

        #Apply fine scale drift to fits:
        traces['z_px_dc']=traces['z_px']+traces['z_px_fine']
        traces['y_px_dc']=traces['y_px']+traces['y_px_fine']
        traces['x_px_dc']=traces['x_px']+traces['x_px_fine']
        

        #Transform to global coordinates in the ref_frame reference.
        merged_traces_drifts = pd.merge(traces, self.drift_table, left_on = ['position','ref_frame'], right_on=['position', 'frame'])
        merged = pd.merge(merged_traces_drifts, self.roi_table, left_on=['roi_id'], right_index=True)

        traces['z'] = traces['z_px_dc'] + merged['z_min_y'] +  merged['z_px_course_y'] 
        traces['y'] = traces['y_px_dc'] + merged['y_min_y'] +  merged['y_px_course_y']
        traces['x'] = traces['x_px_dc'] + merged['x_min_y'] +  merged['x_px_course_y']

        traces = traces.sort_values(['trace_id', 'frame'])

        #self.image_handler.traces = traces
        traces.to_csv(self.traces_path)