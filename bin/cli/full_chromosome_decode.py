
"""
Created by:

Kai Sandvold Beckwith
Ellenberg group
EMBL Heidelberg

"""

from looptrace.ImageHandler import ImageHandler
import os
import argparse
import tqdm

from skimage.feature import peak_local_max
from looptrace import image_io
from looptrace.gaussfit import fitSymmetricGaussian3D
from looptrace import image_processing_functions as ip
#from looptrace import trace_analysis_functions as tr
import numpy as np
import pandas as pd
import scipy.ndimage as ndi

from sklearn.cluster import SpectralClustering
from sklearn import preprocessing
from sklearn.neighbors import LocalOutlierFactor

def affine_transform_points(points, A):
    ''' Adapted from: https://stackoverflow.com/questions/20546182/how-to-perform-coordinates-affine-transformation-using-python-part-2, user Hunse reply.
        Pad the data with ones, so that our transformation can do translations too

    Args:
        points (_type_): nD point set to transform
        A (_type_): affine matrix

    Returns:
        x_transformed: point set transformed by affine matrix
    '''

    pad = lambda x: np.hstack([x, np.ones((x.shape[0], 1))])
    unpad = lambda x: x[:,:-1]
    transform = lambda x: unpad(np.dot(pad(x), A))

    return transform(points)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Extract experimental PSF from bead images.')
    parser.add_argument("--config_path", help="Experiment config file path")
    parser.add_argument("--image_path", help="Path to folder with images to read.")
    #parser.add_argument('--spot_images_name', help='Name of images in image_path to use as template')
    parser.add_argument('--pos_id', help='(Optional) Index of position to trace.', default=0)
    parser.add_argument('--lib_names', help='Names of libraries to trace', nargs='+')
    parser.add_argument('--sample_roi', help='Provide to decode a single given ROI', default=None)
    
    args = parser.parse_args()
    
    try:
        array_id = int(os.environ["SLURM_ARRAY_TASK_ID"]) #Split datasets by positions if running across SLURM cluster.
        H = ImageHandler(config_path=args.config_path, image_path=args.image_path, pos_id = int(array_id))
        pos_id = int(array_id)
    except KeyError:
        array_id = None
        H = ImageHandler(config_path=args.config_path, image_path=args.image_path, pos_id = int(args.pos_id))
        pos_id = args.pos_id

    imgs = H.images[H.config['trace_input_name']]
    drifts = H.tables[H.config['spot_input_name']+'_drift_correction']
    frame_names = H.config['frame_name']

    #file_ids = [int(s.split('_')[1]) for s in  imgs.files]
    chrom_rois = H.tables[H.config['spot_input_name']+'_rois']
    affine_matrix = np.load(H.out_path+H.config['chrom_abb_image_name']+'_frame_'+str(H.config['chrom_abb_frame'])+'_ref_ch_'+str(H.config['chrom_abb_reference_channel'])+'_mov_chs_'+str([H.config['chrom_abb_corr_channels']] if type(H.config['chrom_abb_corr_channels']) != list else H.config['chrom_abb_corr_channels'])+'_affine.npy')[0]
    all_probes = pd.read_csv(H.config['probe_path'], index_col=0)


    all_probes['trace_bc_name'] = 'Dp'+all_probes['trace_bc_name'].str.slice(2,None).str.zfill(3) #Convert barcode to three digit format.
    all_probes['seg_bc_name'] = 'Dp'+all_probes['seg_bc_name'].str.slice(2,None).str.zfill(3) #Convert barcode to three digit format.
    
    reg_bc_map = H.config['reg_bc_map']
    interfering_libs = H.config['full_chr_interfering_libs']
    #reg_bc_map = {'D6':'Chr2_full', 'D2':'Chr2_1MB', 'D5':'Chr5_1MB', 'D7':'Chr2_12Mb', 'D8':'Chr14_full', 'D11': 'Chr14_11Mb'}
    #reg_bc_map = {'Dp150':'Chr2_1MB', 'Dp152':'Chr5_1MB', 'Dp153':'Chr2_full'} #This is old mapping, only use for older datasets.
    position = sorted(list(chrom_rois.position.unique()))[pos_id]

    for lib_name in args.lib_names:
        print('Tracing ', lib_name, ' in position ', position)
        probes = all_probes[all_probes.lib_name ==  lib_name]

        trace_ch = int(H.config['full_chr_trace_ch'])#0
        seg_ch = int(H.config['full_chr_seg_ch'])#0
        reg_bc_ch = int(H.config['full_chr_reg_bc_ch'])#1
        bead_roi_px = int(H.config['full_chr_roi_px'])#5


        trace_frames = np.argwhere(np.isin(frame_names['ch_'+str(trace_ch)], probes.trace_bc_name.unique())).T[0]
        seg_frames = np.argwhere(np.isin(frame_names['ch_'+str(seg_ch)], probes.seg_bc_name.unique())).T[0]
        blank_frames = np.argwhere(np.isin(frame_names['ch_'+str(trace_ch)], ['blank'])).T[0]
        current_lib = np.argwhere(np.isin(frame_names['ch_'+str(reg_bc_ch)], list(reg_bc_map.keys())[list(reg_bc_map.values()).index(lib_name)])).T[0]
        other_libs = np.setdiff1d(np.argwhere(np.isin(frame_names['ch_'+str(reg_bc_ch)], list(reg_bc_map.keys()))).T[0], current_lib)
        interfering_libs = np.argwhere(np.isin(frame_names['ch_'+str(reg_bc_ch)], [k for k,v in reg_bc_map.items() if v in interfering_libs])).T[0]
        print('Interfering lib', interfering_libs)
        probes['combos'] = probes['trace_bc_name'] + '_' + probes['seg_bc_name']
        probes['combos_gpos'] = probes.combos.map(probes.groupby('combos').start.mean().to_dict())
        lib_combos = np.unique(probes['combos'])
        #print(lib_combos)
        #nuc_class_map = {0:'exclude', 1:'inter', 2:'pro', 3:'prometa', 4:'meta', 5:'ana', 6:'telo', 7:'early_g1'}
        #chrom_rois['phase'] = chrom_rois['nuc_class'].map(nuc_class_map)

        chrom_rois.loc[:,'reg_bc_name'] = chrom_rois.apply(lambda row: frame_names['ch_'+str(reg_bc_ch)][row['frame']], axis=1)
        chrom_rois.loc[:,'region'] = chrom_rois['reg_bc_name'].map(reg_bc_map)
        chrom_rois_sel = chrom_rois[(chrom_rois.region == lib_name) & (chrom_rois.position == position)]
        #print(chrom_rois)
        #median_area = chrom_rois_sel.area.median()
        if args.sample_roi is not None:
            all_rois = [int(args.sample_roi)]
        else:
            all_rois = list(chrom_rois_sel.index)
        for roi_id in tqdm.tqdm(all_rois):
            
            current_roi = chrom_rois.loc[roi_id]
            print(roi_id, current_roi['label'])
            #sel_roi = (sel_roi_max-sel_roi_min)/2 + sel_roi_min

            fine_drift_pos = drifts[drifts.position==current_roi['position']]
            dc_img = np.zeros(shape=(imgs[roi_id].shape))
            for i in range(imgs[roi_id].shape[0]):
                dc_img[i] = ndi.shift(imgs[roi_id][i], np.array([0]+fine_drift_pos.iloc[i][['z_px_fine', 'y_px_fine', 'x_px_fine']].to_list()), order=1)

            all_peaks = []
            peak_id = 0
            for t in tqdm.tqdm(trace_frames):
                t_img = dc_img[t,trace_ch]
                blank_dc = dc_img[blank_frames[-1],trace_ch]
                blank_dc_mask = (blank_dc<np.median(blank_dc)*1.5)
                median = np.median(imgs[roi_id])
                #peaks = peak_local_max(t_img*blank_dc_mask, min_distance=1, threshold_abs=median*2) #With removal of beads
                peaks = peak_local_max(t_img, min_distance=1, threshold_abs=median*2) #Without removal of beads
                for peak in peaks:
                    peak_img = ip.extract_single_bead(peak, t_img, bead_roi_px = bead_roi_px)
                    fit = fitSymmetricGaussian3D(peak_img, sigma=1,  center = None)[0]
                    fit[2:5] = fit[2:5]+np.array(peak)-bead_roi_px//2
                    all_peaks.append(list(peak)+list(fit)+[peak_id, t, frame_names['ch_'+str(trace_ch)][t]])
                    peak_id += 1

            fits_list = []
            for peak in all_peaks:
                av_ints = []
                for j in seg_frames:
                    av_int = dc_img[j, seg_ch][peak[0], peak[1], peak[2]]
                    fits_list.append(peak+[j, frame_names['ch_'+str(seg_ch)][j], av_int])
                for j in current_lib:
                    av_int = dc_img[j, reg_bc_ch][peak[0], peak[1], peak[2]]
                    fits_list.append(peak+[j, frame_names['ch_'+str(reg_bc_ch)][j], av_int])
                for j in interfering_libs:
                    av_int = dc_img[j, reg_bc_ch][peak[0], peak[1], peak[2]]
                    fits_list.append(peak+[j, frame_names['ch_'+str(reg_bc_ch)][j], av_int])

            fits = pd.DataFrame(fits_list, columns=['peak_z', 'peak_y', 'peak_x', 'BG', 'A', 'z', 'y', 'x', 'sigma_z', 'sigma_xy', 'peak_id', 'trace_frame', 'trace_frame_name', 'seg_frame', 'seg_frame_name', 'av_int'])
            fits = fits.reset_index().rename(columns={'index':'fit_id'})

            fits['roi_id'] = roi_id
            fits['position'] = position
            fits['region'] = lib_name

            #Convert to global coordinates for easy overlays
            sel_roi_min = current_roi[['z_min', 'y_min', 'x_min']].to_numpy()
            drift_to_roi = drifts.loc[(drifts.position==current_roi['position']) & ((drifts.frame==current_roi['frame'])), ['z_px_course', 'y_px_course', 'x_px_course']].values[0]
            global_shift = sel_roi_min + drift_to_roi
            #sel_roi_max = current_roi[['z_max', 'y_max', 'x_max']].to_numpy()

            fits[['z']] = fits[['z']] + global_shift[0]
            fits[['y']] = fits[['y']] + global_shift[1]
            fits[['x']] = fits[['x']] + global_shift[2]

            #Chromatic abberation correction (needs to be done on pixels, not nm)
            fits[['z','y','x']] = affine_transform_points(fits[['z','y','x']].to_numpy(), affine_matrix)

            #Convert to nm
            fits.loc[:, ['y','x','sigma_xy']] = fits.loc[:, ['y','x','sigma_xy']]*float(H.config['xy_nm'])
            fits.loc[:, ['z','sigma_z']] = fits.loc[:, ['z','sigma_z']]*float(H.config['z_nm'])

            #Basic quality filtering of fits
            fits['QC'] = 1
            fits.loc[(fits['A']/fits['BG']) < 0.5, 'QC'] = 0
            fits = fits[fits.QC == 1]

            #Filter out likely bead fits that have high values in many frames.
            median = fits.av_int.median()
            median_sum = fits.groupby('peak_id').av_int.sum().median()*2
            fits = fits.groupby('peak_id').filter(lambda x: x.av_int.sum() <median_sum)

            #Remove fits with signal in regional barcode of interfering librarieries:
            if len(interfering_libs)>0:
                fits = fits[fits.peak_id.isin(fits[(fits.av_int<median*2) & (fits.seg_frame.isin(interfering_libs))].peak_id.to_list())]

            #Filter for fits with good signal in whole chromosome label
            fits = fits[fits.peak_id.isin(fits[(fits.av_int>median*2) & (fits.seg_frame.isin(current_lib))].peak_id.to_list())]

            #List all combinations
            fits['combos'] = fits['trace_frame_name'] + '_' + fits['seg_frame_name']
            
            #Filter for fits with legal combinations in library
            fits = fits[fits['combos'].isin(lib_combos)]

            if args.sample_roi is not None:
                path = H.out_path+'full_chromosome_decode_sample_raw_fits_'+args.sample_roi+'.csv'
                fits.to_csv(path, mode='w', header=True)

            #Assign zscores
            try:
                fits['zscore'] = fits.groupby(['peak_id']).apply(lambda x: (x['av_int']-np.mean(x['av_int']))/np.std(x['av_int'])).values
            except ValueError as e:
                print(e)
                continue

            #Apply final quality control
            fits = fits[fits.av_int > median*1.5] #This is in any of the segment channels
            fits = fits[fits['zscore']>0] #This is in any of the segment channels

            fits['g_pos'] = fits.combos.map(dict(zip(probes.combos,probes.combos_gpos)))
            fits = fits.sort_values('g_pos').reset_index(drop=True)

            if len(fits) < 10:
                print('Short, aborting.')
                continue

            #Feature vector for clustering of traces:
            X = fits[['z', 'y', 'x', 'g_pos']].to_numpy()

            X = preprocessing.RobustScaler().fit_transform(X) #Scale data for clustering
            ifc = LocalOutlierFactor().fit_predict(X) #Try to remove some outliers
            #print(sum(ifc==0)) #
            fits = fits.iloc[ifc.astype(bool),:] #Remove outliers from fits df.

            success=False #Try different number of spectral clusters until assignments are less ambiguous, this occurs if multiple chromosomes overlap:
            for n in range(1,5):
                    sc =    SpectralClustering(
                            n_clusters=n,
                            eigen_solver="arpack",
                            affinity="nearest_neighbors",
                            random_state=42,).fit_predict(X) 

                    fits['clust'] = sc
                    mean_combo_assigments = fits.groupby(['clust', 'combos'])['peak_id'].count().quantile(q=0.75) #Check number of ambiguous assigments
                    if mean_combo_assigments == 1:
                            success=True
                            print('Success', n, 'g_pos')
                            break

            if not success: #Occasionally assigmnets remain ambigous when using genomic positions in the features, try again without:
                    X = fits[['z', 'y', 'x']].to_numpy()
                    for n in range(1,5):
                            sc =    SpectralClustering(
                                    n_clusters=n,
                                    eigen_solver="arpack",
                                    affinity="nearest_neighbors",
                                    random_state=42,).fit_predict(X)

                            fits['clust'] = sc
                            mean_combo_assigments = fits.groupby(['clust', 'combos'])['peak_id'].count().quantile(q=0.75)
                            if mean_combo_assigments == 1:
                                    success=True
                                    print('Success', n, 'coords')
                                    break
            print(success, n)



            #print(len(fits))
            fits = fits.loc[fits.groupby(['roi_id', 'clust', 'peak_id']).zscore.idxmax()].reset_index(drop=True) #Extract final fits with maximum zscore to assign final position
            fits['tmp'] = fits['roi_id'].astype(str) + '_' + fits['clust'].astype(str) #Assign unique trace_ids
            fits['trace_id'] = fits['tmp'].map({tmp:i for i, tmp in enumerate(fits.tmp.unique())})
            fits = fits.drop(columns = ['tmp'])
            fits = fits.sort_values(['trace_id', 'g_pos'])

            # Assign missing positions n/a values for later analysis.
            new_groups = []
            for lib in fits.region.unique():
                sel_traces = fits[fits.region == lib]
                lib_gpos = set(probes.combos_gpos)
                for trace_id in tqdm.tqdm(sel_traces.trace_id.unique()):
                    current_trace = sel_traces.query('trace_id == @trace_id')#[sel_traces.trace_id == trace_id]
                    sample_row = current_trace.iloc[[0],:]

                    missing_gpos = sorted(list(lib_gpos.difference(set(current_trace.g_pos))))
                    n_missing_pos = len(missing_gpos)

                    if n_missing_pos > 1:
                        new_group = pd.DataFrame(np.repeat(sample_row.values, n_missing_pos, axis=0), columns=sample_row.columns)
                        new_group['g_pos'] = missing_gpos
                        new_group['QC'] = 0
                        new_groups.append(new_group)

                    elif n_missing_pos == 1:
                        new_group = sample_row
                        new_group['g_pos'] = missing_gpos
                        new_group['QC'] = 0
                        new_groups.append(new_group)

                    else:
                        pass
                    #print(new_group)
            fits_out = pd.concat([fits,pd.concat(new_groups)]).sort_values('zscore', ascending=False).drop_duplicates(subset=['trace_id','g_pos'], keep='first').sort_values(['trace_id','g_pos'])

            if args.sample_roi is not None:
                path = H.out_path+'full_chromosome_decode_sample_'+args.sample_roi+'.csv'
                fits_out.to_csv(path, mode='w', header=True)
            else:
                if H.pos_id is not None:
                    path = H.out_path+'full_chromosome_decode_'+str(H.pos_id).zfill(4)+'.csv'
                    if os.path.exists(path):
                        fits_out.to_csv(path, mode='a+', header=False)
                    else:
                        fits_out.to_csv(path, mode='a+', header=True)
                else:
                    path = H.out_path+'full_chromosome_decode.csv'
                    if os.path.exists(path):
                        fits_out.to_csv(path, mode='a+', header=False)
                    else:
                        fits_out.to_csv(path, mode='a+', header=True)



