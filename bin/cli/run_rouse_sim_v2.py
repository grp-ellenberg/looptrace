"""
Created by:

Kai Sandvold Beckwith
Ellenberg group
EMBL Heidelberg
"""

from looptrace import rouse_polymer
import sys
import os
import json
import argparse
from looptrace import rouse_polymer
import numpy as np
import rouse
import tqdm

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run SMC loop extrusion simulation and rouse polymer simulation with updated rouse package (pipy rouse).')
    parser.add_argument("--folder", help="JSON parameters file path", default=None)
    args = parser.parse_args()
    try:
        array_id = int(os.environ["SLURM_ARRAY_TASK_ID"])
    except KeyError:
        array_id = 0
    
    #folder_list = ['MEOX1', 'HIST1', 'YARS2', 'WAPL', 'lncRNA', 'Chr2-A', 'Chr4-B', 'HoxA', 'SHH']
    #folder_list = ['MEOX1_dWAPL', 'HIST1_dWAPL', 'YARS2_dWAPL', 'WAPL_dWAPL', 'lncRNA_dWAPL', 'Chr2-A_dWAPL', 'Chr4-B_dWAPL', 'HoxA_dWAPL', 'SHH_dWAPL']
    folder_list = ['Chr2_1MB','Chr5_1MB']

    if args.folder is not None:
        folder = args.folder
    else:
        folder = folder_list[array_id]

    with open('/g/ellenberg/Kai/Trace_analysis_comparison/rouse_simulations/updated_model_oct23_crash/'+folder+'/params.json', "r") as f:
        params = json.load(f)
    rouse_polymer.run_SMC_sim_random_halt(params)
    loops = np.load('/g/ellenberg/Kai/Trace_analysis_comparison/rouse_simulations/updated_model_oct23_crash/'+folder+'/SMC_pos_0.npy')
    all_loops = (loops - params['smc_pad_size']//1000).astype(np.int64)
    loop_list = [all_loops[i][np.all(all_loops[i]>0, axis = 1) & np.all(all_loops[i]<params['size_polymer_sim'], axis=1)] for i in range(all_loops.shape[0])]

    N=params['size_polymer_sim']
    conf = []
    for i in tqdm.tqdm(range(len(loop_list))):
        model = rouse.Model(N=N, d=3, D = 3.4e3, k = 12)
        bonds = [tuple(np.round(j).astype(int)) for j in loop_list[i]]
        try:
            model.add_bonds(bonds, k_rel=10)
        except IndexError:
            pass

        conf.append(np.stack([model.conf_ss() for k in range(10)]))
    conf = np.concatenate(conf, axis=0)
    np.save('/g/ellenberg/Kai/Trace_analysis_comparison/rouse_simulations/updated_model_oct23_crash/'+folder+'/conf_0.npy', conf)