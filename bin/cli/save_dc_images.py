
"""
Created by:

Kai Sandvold Beckwith
Ellenberg group
EMBL Heidelberg
"""

from looptrace.ImageHandler import ImageHandler
from looptrace.Drifter import Drifter
import os
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Extract experimental PSF from bead images.')
    parser.add_argument("--config_path", help="Config file path")
    parser.add_argument("--image_path", help="Path to folder with images to read.")
    parser.add_argument("--image_save_path", help="(Optional): Path to folder to save images to.", default=None)
    parser.add_argument('--image_name', help = '(Optional). Provide image_name that should be saved with drift correction, overrides reg_image_moving in config.', default=None)
    parser.add_argument('--pos_id', help = '(Optional) Only run for a given position.', default = None)
    parser.add_argument('--max_project', help = 'Save maximum projections along z-axis instead of 3D stack.', action='store_true')
    args = parser.parse_args()

    if args.pos_id is None:
        try:
            array_id = int(os.environ["SLURM_ARRAY_TASK_ID"])
        except KeyError:
            array_id = None
    else:
        array_id = args.pos_id

    H = ImageHandler(config_path=args.config_path, image_path=args.image_path, image_save_path=args.image_save_path, pos_id = int(array_id))
    D = Drifter(H)
    if args.max_project:
        D.save_proj_dc_images(image_name=args.image_name)
    else:
        D.save_dc_images(image_name=args.image_name)