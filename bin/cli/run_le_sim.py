import numpy as np
from numpy.random import default_rng
rng = default_rng()
import pandas as pd
import json
import os
from looptrace import rouse_polymer
import random
import glob

def setup_sim(folder):
    folder_name = 'Cond_I_II_passing_3kbps_dist'
    region_name = 'Chr5_1MB'

    params = {}
    params['size_smc_sim'] = 100000 #in kbp
    params['smc_init_steps'] = int(0)#int(5e3)
    params['smc_steps'] = int(3600)

    params['CTCF_sites'] = list(range(35000,45000,200))
    params['n_CTCF'] = int(2*len(CTCF_sites))
    params['CTCF_site_probability'] = list(np.ones(len(params['CTCF_sites']))/len(params['CTCF_sites']))#(CTCF_sites['occupancy']/np.sum(CTCF_sites['occupancy'])).to_list()#((CTCF_sites['occupancy']/params['n_CTCF'])/np.sum((CTCF_sites['occupancy']/params['n_CTCF']))).to_list() #Needs to be exactly normalized to 1.0, otherwise simulation might segfault with numba random choice implementation.
    params['CTCF_site_direction'] = list([random.choice([-1, 1]) for i in range(len(params['CTCF_sites']))])#[1 if s == '+' else -1 for s in CTCF_sites['strand']]
    params['CTCF_bound_lifetime'] = 0
    params['CTCF_unbound_lifetime'] = 1
    params['CTCF_SMC_bound_lifetime'] = 1

    params['n_SMC'] = int(params['size_smc_sim']*(12/1e3)) #Total number of complexes in simulation
    params['SMC_types'] = 2
    params['SMC_distribution'] = [.8, .2] #Ratio of total
    params['SMC_bound_lifetime'] = [0, 0]#[150,2400] for Condensin I and II, [900 480] for STAG1, STAG2 #Lifetime of binding
    params['SMC_unbound_lifetime'] = [1, 1] #Lifetime of unbound (exponential relation to bound fraction)
    params['SMC_CTCF_bound_lifetime'] = [0, 0]#[18000, 900] #Lifetime of CTCF interaction
    params['SMC_crash_lifetime'] = [30,30]#[30, 30] #Lifetime of collision
    params['extrusion_sided'] = [1, 1] #One or two-sided extrusion
    params['extrusion_rate'] = [3, 3] #Extrusion rate per step of each arm
    params['loop_lifetime'] = [900, 480] #Not implemented
    params['loop_force'] = 1 #bond strength of loops
    params['SMC_crash_prob'] = [0,0]#[1.,1.] #Probability that overlapping positions between extruder arms will lead to stalling

    params['out_path'] = folder
    #params['out_path_win'] = r'M:\Kai\notebooks\polychrom\mitofold\\'+folder_name
    params['params_path'] = folder+os.sep+'params.json'
    
    if not os.path.isdir(params['out_path']):
        os.makedirs(params['out_path'])
    #with open(params['params_path'], "w") as outfile:
    #    json.dump(params, outfile)

    #Initialization step
    params['smc_init_steps'] = int(2000)
    params['smc_steps'] = int(0)
    params['extrusion_rate'] = [0, 0]
    params['SMC_bound_lifetime'] = [0, 24000]
    params['SMC_unbound_lifetime'] = [1, 1]
    with open(folder+os.sep+'params_init.json', "w") as outfile:
        json.dump(params, outfile)

    #Prophase 
    params['smc_init_steps'] = int(0)
    params['smc_steps'] = int(600)
    params['SMC_bound_lifetime'] = [0, 24000]
    params['SMC_unbound_lifetime'] = [1, 1]
    params['extrusion_rate'] = [3, 3]
    with open(folder+os.sep+'params_run_0.json', "w") as outfile:
        json.dump(params, outfile)

    #Prometaphase and metaphase
    params['smc_steps'] = int(1800)
    params['SMC_bound_lifetime'] = [150, 24000]
    with open(folder+os.sep+'params_run_1.json', "w") as outfile:
        json.dump(params, outfile)


def run_sim(folder):

    #Initialization step
    with open(folder+os.sep+'params_init.json', "r") as param_file:
        params = json.load(param_file)
    CTCFs, SMCs = rouse_polymer.run_SMC_sim(params, run_id = 0)

    #Prophase (overwrites output from init step which is not used)
    with open(folder+os.sep+'params_run_0.json', "r") as param_file:
        params = json.load(param_file)
    CTCFs, SMCs = rouse_polymer.run_SMC_sim(params, run_id = 0, continue_previous=True, CTCFs=CTCFs, SMCs=SMCs)

    #Prometaphase and metaphase
    with open(folder+os.sep+'params_run_1.json', "r") as param_file:
        params = json.load(param_file)
    CTCFs, SMCs = rouse_polymer.run_SMC_sim(params, run_id = 1, continue_previous=True, CTCFs=CTCFs, SMCs=SMCs)

def export_results(folder):
    arr = []
    for p in tqdm.tqdm(glob.glob(folder+os.sep+'SMC_pos_*.npy')):
        res_loops = np.load(p,mmap_mode='r')
        arr.append(res_loops)
    arr = np.concatenate(arr, axis = 0)
    arr = np.clip(arr, 1, None)
    #arr = np.concatenate([arr, np.tile(arr[-1], (1000,1,1))], axis=0)

    ### Save SMC steps into polychrom HDF5 format. Only save every tenth step to reduce simulation time
    import h5py
    with h5py.File(folder+os.sep+'LEFPositions.h5', mode='w') as myfile:
        dset = myfile.create_dataset("positions", 
                                    shape=(arr.shape[0]//10, arr.shape[1], arr.shape[2]), 
                                    dtype=np.int32, 
                                    compression="gzip")
        dset[:] = arr[::10]
        myfile.attrs["N"] = params['size_smc_sim']
        myfile.attrs["LEFNum"] = arr.shape[1]

def sim_results(folder):

    from matplotlib import pyplot as plt
    for p in tqdm.tqdm(glob.glob(folder+os.sep+'SMC_pos_*.npy')):
        res_loops = np.load(p,mmap_mode='r')
        arr.append(res_loops)
    arr = np.concatenate(arr, axis = 0)

    loops = np.zeros((len(arr), np.max(arr)))
    for row, i in enumerate(arr):
        try:
            for j in i:
                for k in j:
                    loops[row,k] = 1
                    loops[row,k-1] = 1
                    loops[row,k+1] = 1
        except (TypeError, IndexError):
            pass

    fig, ax = plt.subplots(1, 1,figsize=(20, 20))
    start = np.random.randint(0, len(res_loop_pos))
    ax.imshow(loops[0:10000, :], aspect="auto", cmap='gnuplot2', vmin=0, vmax=1)
    ax.set_xlim(2000,6000)
    plt.savefig(folder+os.sep+'LE_progression.png')
    # ax[1].plot(np.sum(arr[:, :], axis=0))
    # ax[1].set_xlim(2000,6000)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Setup and run loop extrusion simulation.')
    parser.add_argument("--folder_path", help="Folder with simulation files")
    parser.add_argument("--nosetup", help='Skip setup stage to run simulation from existing parameter files', action='store_true')
    parser.add_argument("--view_le", help='Exports a PNG file of a sample of the loop extrusion progression', action='store_true')
    args = parser.parse_args()
    if not args.nosetup:
        setup_sim(args.folder_path)
    run_sim(args.folder_path)
    if args.view_le:
        sim_results(args.folder_path)